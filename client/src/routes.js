import React from 'react';


const Appliances = React.lazy(() => import('./pages/appliances/Appliances'));
const AppliancesDriver = React.lazy(() => import('./pages/appliances/AppliancesDriver'));

const routes = [
  { path: '/appliances', exact: true, name: 'Appliances', component: Appliances },
  { path: '/appliances/create', exact: true,  name: 'Appliances Create', component: AppliancesDriver },
  { path: '/appliances/edit/:id', exact: true, name: 'Appliances Edit', component: AppliancesDriver }
];

export default routes;
