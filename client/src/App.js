import React, { Component } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import './css/styles.css';

const loading = (
  <div className="col-xs-12 text-center">
    <div className="spinner-border text-primary"></div>
  </div>
)

// Containers
const Layout = React.lazy(() => import('./containers/Layout'));

// Pages
const Page404 = React.lazy(() => import('./pages/page404/Page404'));

class App extends Component {

  render() {
    return (
      //Basic routing; Pending login route goes here.
      <HashRouter>
          <React.Suspense fallback={loading}>
            <Switch>
              <Route path="/" render={props => <Layout {...props}/>} />
              <Route name="Page 404" render={props => <Page404 {...props}/>} />
            </Switch>
          </React.Suspense>
      </HashRouter>
    );
  }
}

export default App;
