export const BASE_API_URI = "http://localhost:4000/api";

//rest api routes constants
export const TYPES_URI = BASE_API_URI + "/types";
export const APPLIANCES_URI = BASE_API_URI + "/appliances";
