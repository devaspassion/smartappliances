import React from 'react';
import { render, screen } from '@testing-library/react';
import Appliances from '../pages/appliances/Appliances';
import { Provider } from 'react-redux'
import store from '../store'

it('appliances component renders correctly', () => {
    render(<Provider store={store}>
        <Appliances />
    </Provider>)
  expect(screen.getByRole('table')).toBeTruthy()
  expect(screen.getByRole('button')).toHaveTextContent('Create')
});
