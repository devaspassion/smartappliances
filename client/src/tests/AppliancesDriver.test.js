import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import AppliancesDriver from '../pages/appliances/AppliancesDriver';
import { Provider } from 'react-redux'
import store from '../store'

jest.mock('react-router-dom', () => ({
  useHistory: () => ({
    push: jest.fn(),
  }),
  useLocation: () => ({
    push: jest.fn(),
  }),
}));


it('appliances operations component renders correctly', () => {
    const { queryByPlaceholderText } = render(
        <Provider store={store}>
            <AppliancesDriver />
        </Provider>)
    async () => {
        expect(await screen.findByPlaceholderText('Appliance Type')).toBeTruthy()
        expect(await screen.findByPlaceholderText('Brand Name')).toBeTruthy()
        expect(await screen.findByPlaceholderText('Model No.')).toBeTruthy()
        expect(await screen.findByPlaceholderText('Voltage')).toBeTruthy()
        expect(await screen.findByPlaceholderText('Color')).toBeTruthy()
        expect(await screen.findByPlaceholderText('States')).toBeTruthy()
        expect(await screen.findByPlaceholderText('Close Button')).toBeTruthy()
    }
});

describe("Input Value", ()=>{
  
    it("updates on change", () => {
        const { queryByPlaceholderText } = render(
            <Provider store={store}>
                <AppliancesDriver />
            </Provider>)
        async () => {
            expect(await screen.findByPlaceholderText('Value = "SA001"')).toBeInTheDocument()
            const appType = queryByPlaceholderText('Appliance Type')
            fireEvent.change(appType, { target: { value: "" } })
            expect(appType.value).toBe("")
        }
      
    })

})