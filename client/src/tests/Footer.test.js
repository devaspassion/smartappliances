import { render, screen } from '@testing-library/react';
import Footer from '../containers/Footer';

test('renders footer correctly', () => {
  render(<Footer />);

  expect(screen.getByRole("row")).toBeTruthy()
  expect(screen.getByRole("contentinfo")).toHaveTextContent('Copyright')
});
