import { render, screen } from '@testing-library/react';
import Header from '../containers/Header';

test('renders header correctly', () => {
  render(<Header />);

  expect(screen.getByRole("banner")).toBeTruthy()
  expect(screen.getByRole("contentinfo")).toHaveTextContent('SMARTAPPLIANCES')
});
