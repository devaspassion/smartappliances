import React, { Suspense } from 'react'
import { Container } from 'react-bootstrap';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import {
  Redirect,
  Route,
  Switch
} from 'react-router-dom';

// routes config
import routes from '../routes';

const Page404 = React.lazy(() => import('../pages/page404/Page404'));
  
const loading = (
  <div className="col-xs-12 text-center">
    <div className="spinner-border text-primary"></div>
  </div>
)

/**
 * Content container wrapper with internal routing. 
 * Structured as routes for pages after login.
 * 
 */
const Content = () => {
  return (
    <main className="main">
          <Container fluid>
              <ToastContainer />
        <Suspense fallback={loading}>
          <Switch>
            {routes.map((route, idx) => {
              return route.component && (
                <Route
                  key={idx}
                  path={route.path}
                  exact={route.exact}
                  name={route.name}
                  render={props => (
                    <div>
                      <route.component {...props} />
                    </div>
                  )} />
              )
            })}
            <Route exact path="/" render={() => <Redirect to="/appliances" />} />
            <Route name="Page 404" render={props => <Page404 {...props}/>}  />
          </Switch>
        </Suspense>
      </Container>
    </main>
  )
}

export default React.memo(Content)
