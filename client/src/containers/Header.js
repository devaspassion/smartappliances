import React from 'react';
import { useHistory } from 'react-router-dom';
import { BiAward } from "react-icons/bi";

const Header = () => {
  const history = useHistory();
  return (
    <div> 
    <header className="main-header" role="banner">
    <span onClick={() => history.push('/appliances')}>
      <BiAward className="logo-icon" />
      <span className="logo-txt" role="contentinfo" > SMARTAPPLIANCES</span>
    </span>
    </header>
  </div>
  )
}

export default Header
