import React from 'react'
import { useHistory } from 'react-router-dom';

const Footer = () => {
  const history = useHistory();
  return (
    <footer className="main-footer" role="row">
    <div className="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong role="contentinfo">Copyright &copy; 2021 <span onClick={() => history.push('/appliances')} className="foot-link">SMARTAPPLIANCES</span></strong></footer>
  )
}

export default React.memo(Footer)
