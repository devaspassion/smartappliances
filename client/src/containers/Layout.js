import React from 'react'
import {
  Content,
  Footer,
  Header
} from './index'

const Layout = () => {

  return (
    <div className="wrapper">
      <Header/>
      <div className="content-wrapper" style={{minHeight:window.innerHeight-115}}>
        <div className="content">
          <Content/>
        </div>
      </div>
      <Footer/>
    </div>
  )
}

export default Layout
