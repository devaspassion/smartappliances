export default {
    appliances(url) {
        return {
          getOne: ({ id })   => axios.get(`${url}/${id}`),
          getAll: ()         => axios.get(url),
          create: (toCreate) =>  axios.post(url,toCreate),
          update: (toUpdate) =>  axios.put(url,toUpdate),
          delete: ({ id })   =>  axios.delete(`${url}/${id}`)
        }
      }
    }