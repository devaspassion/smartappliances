import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {Table, Button} from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { TYPES_URI, APPLIANCES_URI } from '../../constants';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import { Multiselect } from 'multiselect-react-dropdown';
import SweetAlert from 'react-bootstrap-sweetalert';
import LoadingBar from 'react-top-loading-bar'
import { BiAddToQueue, BiEdit, BiTrash, BiShare } from "react-icons/bi";

const Appliances = () => {
    const history = useHistory();
    const [types, setTypes] = useState({});
    const [showDeleteAlert, setDeleteConfirmation] = useState(false);
    const [selectedAppId, setAppId] = useState("");
    const applianceStates = useSelector(state => state.states);
    let [progress, setProgress] = useState(0);
    let appliancesLists = useSelector(state => state.appliancesLists );
    const dispatch = useDispatch();
    
    useEffect(() => {
        
        async function fetchAPI() {
            showLoading(true);
          await axios(TYPES_URI)
              .then((reponse) => {
                   showLoading(false);
                  reponse.data = reponse.data.reduce((result, { type_code, type_name }) => {
                      result[type_code] = type_name;
                      return result;
                  }, {});
                  setTypes(reponse.data || []);

              })
              .catch((error) => {
                  showLoading(false);
                  toast.error(error.message || "Error! Request Failed.", {
                      position: toast.POSITION.TOP_RIGHT
                  });
              });
            showLoading(true);
          await axios(APPLIANCES_URI)
              .then((reponse) => {
                  showLoading(false);
                  appliancesLists = [...reponse.data.appliances]
                  dispatch({ type: 'set', appliancesLists });
                })
              .catch((error) => {
                  showLoading(false);
                    toast.error(error.message || "Error! Request Failed.", {
                        position: toast.POSITION.TOP_RIGHT
                    });
                });
        };
        fetchAPI();
    }, []);

    useSelector(state => state);
    const validateStateSelect = function (selectedList, selectedItem) {
        if (selectedItem.name === "RUNNING") {
            if (!selectedList.find(item => item.name === 'ON')) {
                const foundIndex = selectedList.findIndex(item => item.id === selectedItem.id)
                foundIndex !== -1 && selectedList.splice(foundIndex, 1);
                toast.error("Running state is allowed only if device is On ", {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        }
        if (selectedItem.name === "OFF") {
            let foundIndex = selectedList.findIndex(item => item.name === "ON");
            foundIndex !== -1 && selectedList.splice(foundIndex, 1);
            foundIndex = selectedList.findIndex(item => item.name === "RUNNING");
            foundIndex !== -1 && selectedList.splice(foundIndex, 1);
        }
        if (selectedItem.name === "ON") {
            let foundIndex = selectedList.findIndex(item => item.name === "OFF");
            foundIndex !== -1 && selectedList.splice(foundIndex, 1);
            foundIndex = selectedList.findIndex(item => item.name === "RUNNING");
            foundIndex !== -1 && selectedList.splice(foundIndex, 1);
        }
        return selectedList;
    }
    const validateStateRemove = function (selectedList, removedItem) {
        if (removedItem.name === "ON") {
            if (selectedList.find(item => item.name === 'RUNNING')) {
                selectedList.splice(0, 0, removedItem);
                toast.error("On state cannot be removed for a Running device.", {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        }
        return selectedList;
    }
    const showDeleteConfirm = (app_model, showFlag) => {
        setAppId(app_model);
        setDeleteConfirmation(showFlag);
    }
    const removeAppliance = () => {
        showLoading(true);
        setDeleteConfirmation(false);
        axios.delete(APPLIANCES_URI, { data: { app_model: selectedAppId } })
            .then((reponse) => {
                showLoading(false);
                appliancesLists.map((appItem, index) => {
                    if (appItem.app_model === selectedAppId)
                        return appliancesLists.splice(index, 1);
                })
                dispatch({ type: 'set', appliancesLists });
                toast.success("Deleted Successfully.", {
                    position: toast.POSITION.TOP_RIGHT
                });
            })
            .catch((error) => {
                showLoading(false);
                toast.error(error.message || "Error! Request Failed.", {
                    position: toast.POSITION.TOP_RIGHT
                });
            });
    }
    const setAppliancesStates = (appModelId) => {
        let appStates = [];
        showLoading(true);
        appliancesLists.forEach((appItem, index) => {
            if (appItem.app_model === appModelId) appStates = appItem.app_states;
        });
        axios.patch(APPLIANCES_URI, { app_model: appModelId, app_states: appStates })
            .then((reponse) => {
                showLoading(false);
                toast.success("Updated Successfully.", {
                    position: toast.POSITION.TOP_RIGHT
                });
            })
            .catch((error) => {
                showLoading(false);
                toast.error(error.message || "Error! Request Failed.", {
                    position: toast.POSITION.TOP_RIGHT
                });
            });
    }

    const showLoading = (show) => {
        if (show) {
            progress = progress + 1;
            if (progress <= 100) {
                setProgress(progress);
                showLoading(true);
            }
        } else {
            setProgress(0);
        }
    }
    
  return (
      <div>
          <LoadingBar color={"#07ff98"} progress={progress}
              onLoaderFinished={() => setProgress(0)} />
     <Button variant="primary" style={{marginBottom:"10px"}} onClick={() => history.push('/appliances/create')}> <BiAddToQueue /> Create </Button>
     <div className="table-responsive" style={{ minHeight: "500px" }}>
      <Table striped bordered hover id="app-tbl">
        <thead>
          <tr>
            <th>#</th>
            <th>Type</th>
            <th>Brand</th>
            <th>Model No.</th>
            <th>Voltage</th>
            <th>Color</th>
            <th>States</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
       <tbody>
          {
              appliancesLists && appliancesLists.map((appItem,index) => {
                 return (
                     <tr key={appItem.app_model }>
                           <td>{index+1}</td>
                         <td>{types[appItem.app_type_code]}</td>
                           <td>{appItem.app_brand}</td>
                           <td>{appItem.app_model}</td>
                           <td>{appItem.app_voltage}</td>
                           <td>{appItem.app_color}</td>
                           <td>
                             <Multiselect
                                 options={applianceStates}
                                 selectedValues={appItem.app_states}
                                 onSelect={(selectedList, selectedItem) => { appItem.app_states = validateStateSelect(selectedList, selectedItem) }}
                                 onRemove={(selectedList, removedItem) => { validateStateRemove(selectedList, removedItem) }}
                                 displayValue="name"
                             />
                         </td>
                         <td>
                             <Button variant="primary" onClick={() => setAppliancesStates(appItem.app_model)}> <BiShare /> Update </Button>
                         </td>
                         <td><BiEdit className="edit-btn" onClick={() => history.push('/appliances/edit/' + appItem.app_model)} />
                             <BiTrash className="del-btn" onClick={() => showDeleteConfirm(appItem.app_model,true)} /></td>
                       </tr>
                       )
               })
           }
        </tbody>
              </Table>
              <SweetAlert
                  show={showDeleteAlert}
                  warning
                  showCancel
                  confirmBtnText="Yes, delete it!"
                  confirmBtnBsStyle="danger"
                  title="Are you sure?"
                  onConfirm={removeAppliance}
                  onCancel={() => {
                      setDeleteConfirmation(false)
                  }
                  }
                  focusCancelBtn
                    >
                  You want to delete this device model?
                </SweetAlert>
          </div>
      </div>

  )
}

export default Appliances
