import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {Form, Button} from 'react-bootstrap';
import { useHistory, useLocation } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from "react-hook-form";
import { TYPES_URI, APPLIANCES_URI } from '../../constants';
import { toast } from 'react-toastify';
import LoadingBar from 'react-top-loading-bar'
import 'react-toastify/dist/ReactToastify.min.css';
import { Multiselect } from 'multiselect-react-dropdown';
import { BiAddToQueue, BiSelectMultiple, BiChevronLeftCircle } from "react-icons/bi";


const AppliancesDriver = () => {
  const pathName = useLocation().pathname;
  const isEditMode = pathName && pathName.match('/appliances/edit/') ? true : false;
  const appId = pathName ? pathName.split("/").pop():'';
  const { register, handleSubmit, errors } = useForm();
  const history = useHistory();
  const [types, setTypes] = useState([]);
  const applianceStates = useSelector(state => state.states);
  const dispatch = useDispatch();
  let newState = { app_model: "", app_brand: "", app_type_code: "", app_voltage: 0, app_color: "", app_states:[] };
  let [progress, setProgress] = useState(0);

    useEffect(() => {
        async function fetchAPI() {
            showLoading(true);
          await axios(TYPES_URI)
              .then((reponse) => {
                  setTypes(reponse.data || []);
                  showLoading(false);
            })
              .catch((error) => {
                  showLoading(false);
                  toast.error(error.message || "Error! Request Failed.", {
                      position: toast.POSITION.TOP_RIGHT
                  });
              });
            if (isEditMode) {
                showLoading(true);
                await axios(APPLIANCES_URI + "/" + appId)
                    .then((response) => {
                        showLoading(false);
                        if (response && response.data && response.data.appliances[0])newState = response.data.appliances[0];
                        setStates(newState);
                    })
                    .catch((error) => {
                        showLoading(false);
                        toast.error(error.message || "Error! Request Failed.", {
                            position: toast.POSITION.TOP_RIGHT
                        });
                    });
            }
        };
        fetchAPI();
        
    }, []);
    const [form, setStates] = useState(newState);
  
  /**
   * To handle change event in appliances form.
   */
  const updateField = e => {
      setStates({
      ...form,
      [e.target.name]: e.target.value
    });
  };
  /**
   * To handle form submission for appliances.
   */
    const onSubmit = async (data) => {        
        if (isEditMode) {
            await axios.put(APPLIANCES_URI, form)
                .then((reponse) => {
                    dispatch({ type: 'set', form });
                    toast.success("Updated Successfully.", {
                        position: toast.POSITION.TOP_RIGHT
                    });
                })
                .catch((error) => {
                    toast.error(error.message || "Error! Request Failed.", {
                        position: toast.POSITION.TOP_RIGHT
                    });
                });
        } else {
            await axios.post(APPLIANCES_URI, form)
                .then((reponse) => {
                    toast.success("Created Successfully.", {
                        position: toast.POSITION.TOP_RIGHT
                    });
                    setStates(newState);
                })
                .catch((error) => {
                    toast.error(error.message || "Error! Request Failed.", {
                        position: toast.POSITION.TOP_RIGHT
                    });
                });
        }
  };
    const validateStateSelect = (selectedList, selectedItem) => {
        if (selectedItem.name === "RUNNING") {
            if (!selectedList.find(item => item.name === 'ON')) {
                const foundIndex = selectedList.findIndex(item => item.id === selectedItem.id)
                foundIndex !== -1 && selectedList.splice(foundIndex, 1);
                toast.error("Running state is allowed only if device is On ", {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        }
        if (selectedItem.name === "OFF") {
            let foundIndex = selectedList.findIndex(item => item.name === "ON");
            foundIndex !== -1 && selectedList.splice(foundIndex, 1);
            foundIndex = selectedList.findIndex(item => item.name === "RUNNING");
            foundIndex !== -1 && selectedList.splice(foundIndex, 1);
        }
        if (selectedItem.name === "ON") {
            let foundIndex = selectedList.findIndex(item => item.name === "OFF");
            foundIndex !== -1 && selectedList.splice(foundIndex, 1);
            foundIndex = selectedList.findIndex(item => item.name === "RUNNING");
            foundIndex !== -1 && selectedList.splice(foundIndex, 1);
        }
        form.app_states = selectedList;
    }
    const validateStateRemove = (selectedList, removedItem) => {
        if (removedItem.name === "ON") {
            if (selectedList.find(item => item.name === 'RUNNING')) {
                selectedList.splice(0, 0, removedItem);
                toast.error("On state cannot be removed for a Running device.", {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        }
        form.app_states = selectedList;
    }

    const showLoading = (show) => {
        if (show) {
            progress = progress + 1;
            if (progress <= 100) {
                setProgress(progress);
                showLoading(true);
            }
        } else {
            setProgress(0);
        }
    }
    
    return (
        <div className="row">
            <LoadingBar color={"#07ff98"} progress={progress}
                onLoaderFinished={() => setProgress(0)} />
      <div className="col-lg-3 col-md-6 col-sm-8 col-xs-12 app-form-box">
      <Form onSubmit={handleSubmit(onSubmit)}>
                    <Form.Group>
                        <Form.Control as="select" placeholder="Appliance Type" name="app_type_code" id="app_type_code" value={form.app_type_code} onChange={updateField}
                            aria-invalid={errors.type ? "true" : "false"} ref={register({ required: "required" })} disabled={isEditMode}>
               <option value="">Type</option>
               {
                  types.map((typeItem) => {
                     return <option key={typeItem.type_code} value={typeItem.type_code}>{typeItem.type_name}</option>
                  })
               }
            </Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Control type="text" placeholder="Model No." name="app_model" value={form.app_model} onChange={updateField} id="app_model" disabled={isEditMode} aria-invalid={errors.app_model ? "true" : "false"} ref={register({ required: "required" })} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Control type="text" placeholder="Brand Name" value={form.app_brand} onChange={updateField} name="app_brand" id="app_brand" aria-invalid={errors.app_brand ? "true" : "false"}
          ref={register({
            required: "required"
          })}/>
          </Form.Group>
                    
                    <Form.Group>
                        <Form.Control type="number" placeholder="Voltage" name="app_voltage" id="app_voltage" value={form.app_voltage} onChange={updateField} aria-invalid={errors.app_voltage ? "true" : "false"} />
          </Form.Group>
          <Form.Group>
                        <Form.Control type="text" placeholder="Color" name="app_color" id="app_color" value={form.app_color} onChange={updateField} aria-invalid={errors.app_color ? "true" : "false"}/>
          </Form.Group>
          <Form.Group>
                        <Multiselect
                            placeholder = "States"
                            options={applianceStates}
                            onSelect={(selectedList, selectedItem) => { validateStateSelect(selectedList, selectedItem) }}
                            onRemove={(selectedList, removedItem) => { validateStateRemove(selectedList, removedItem) }}
                            selectedValues={form.app_states}
                            displayValue="name"
                        />
          </Form.Group>
          { isEditMode ? (
                            <Button variant="primary" placeholder="Create Button" size="md" type="submit" className="submt-btn">
                            <BiSelectMultiple />{' '}
                              Update
                          </Button>
                         ) : 
                          (
                            <Button variant="primary" placeholder="Update Button" size="md" type="submit" className="submt-btn">
                            <BiAddToQueue />{' '}
                              Create
                            </Button>
                          )
          }
          <Button variant="danger" placeholder="Close Button" size="md" type="button" className="close-btn" role="button" onClick={() => history.push('/appliances')}>
          <BiChevronLeftCircle />{' '}
            Close
          </Button>
        </Form> 
        </div>
        </div>
  )
}

export default AppliancesDriver
