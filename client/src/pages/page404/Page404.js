import React from 'react';
import { Link } from 'react-router-dom';
import notfound from '../../assets/imgs/notfound.gif';

const Page404 = () => {
  return (
    <div className="col-md-12 text-center">
      <Link to="/appliances">
        <img src={notfound} alt="404 NOT FOUND"/>
      </Link>
    </div>
  )
}

export default Page404
