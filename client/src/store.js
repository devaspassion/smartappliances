import { createStore } from 'redux'

const initialState = {
  states: [{ name: "ON", id: 1 }, { name: "RUNNING", id: 2 }, { name: "OFF", id: 3 }],
  appliance: {
      app_model: "",
      app_brand: "",
      app_type_code: "",
      app_voltage: 0,
      app_color: "",
      app_states: ""
  },
  appliancesLists : []
}

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case 'set':
          return { ...state, ...rest }
      case 'setapplist':
          return { ...state.appliancesLists, ...rest }
    default:
      return state
  }
}

const store = createStore(changeState)
export default store