
### Prerequisites:
Install package manager node or yarn.
Install Node version >= 12.0.0


### Database: 
For demo purpose, mongoDB cloud is used. No setup is required.
mongoose ODM is used.


### How to setup server?

#### To install dependencies for server
        cd server
        npm install

#### To build project
        npm run build

#### To run project
        npm run start
	
#### To run project [Development]
        npm run dev

#### To run the test cases
        npm run test


### How to setup client?

#### To install dependencies for client
        cd client
        npm install

#### To run project
        npm start

#### To run the test cases
        npm test


### How to Create an appliance?
Click on the create button.
Add the details on the corresponding textboxes and click create button.
Type,Model No. and Brand name is required.


### How to Edit an appliance?
Click the edit symbol on the right end of the corresponding appliance.
Edit the required details and click update button.


### How to Update the state of an appliance?
Update the state field of the corresponding appliance.
Click the update button.

Example: Turn off washing machine.
	 Select OFF from select dropdown in appliances lists table and click on update button.

### How to Delete an appliance?
Click the delete symbol on the right end of the corresponding appliance.



NB: The app is tested on Windows machine. 
The authentication part is pending using JWT web tokens.
