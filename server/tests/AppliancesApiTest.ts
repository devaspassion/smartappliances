import * as HttpStatus from 'http-status-codes';
import TestEnv from './TestEnv';
import chai = require('chai');
let server = require('../src/app');
import chaiHttp = require('chai-http');
import { doesNotMatch } from 'assert';
//import { assert } from 'console';
var assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);

const { baseUrl } = TestEnv


describe('Appliances API', () => {
    describe('Fetching all appliances', () => {
        it('should respond with a negative status code if the appliance url incorrect', (done) => {
            chai.request(server)
            .get('/api/appliancess')
            .end((err, res) => {
                chai.expect(res).to.have.status(404);
                done()
            });
        })
        it('should respond with a positive status code if the appliance accesed', (done) => {
             chai.request(server)
                .get('/api/appliances')
                .end((err, res) => {
                    chai.expect(res).to.have.status(200);
                    done()
                });
        })
    })

    
    describe('Creating appliance', () => {
        it('should respond with a negative status code if the appliance url incorrect', (done) => {
            chai.request(server)
                 .post('/api/appliancess')
                .end((err, res) => {
                    chai.expect(res).to.have.status(404);
                    done()
                });
        })
        it('should respond with a negative status code if the appliance id is incorrect', (done) => {
            chai.request(server)
                .post('/api/appliances/id')
                .end((err, res) => {
                    chai.expect(res).to.have.status(404);
                    done()
                });
        })
        it('should respond with a negative status code if the appliance model number is empty', (done) => {
            let appliances = {
                app_model: "",
                app_brand: "ELECTROLUX",
                app_type_code: "SA001",
                app_voltage: "50",
                app_color: "red",
                app_states: []
            }
             chai.request(server)
                .post('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                });
        })
        it('should respond with a negative status code if the appliance brand name is empty', (done) => {
            let appliances = {
                app_model: "1",
                app_brand: "",
                app_type_code: "SA001",
                app_voltage: "50",
                app_color: "red",
                app_states: []
            }
             chai.request(server)
                .post('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                });
        })
        it('should respond with a negative status code if the appliance type is empty', (done) => {
            let appliances = {
                app_model: "2",
                app_brand: "ELECTROLUX",
                app_type_code: "",
                app_voltage: "50",
                app_color: "red",
                app_states: []
            }
             chai.request(server)
                .post('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                });
        })
        it('should respond with a negative status code if the appliance state is ON and OFF', (done) => {
            let appliances = {
                app_model: "3",
                app_brand: "ELECTROLUX",
                app_type_code: "SA001",
                app_voltage: "50",
                app_color: "red",
                app_states: [{"id":1,"name":"ON"}, {"id":3,"name":"OFF"}]
            }
             chai.request(server)
                .post('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                })
        })
        it('should respond with a negative status code if the appliance state is RUNNING', (done) => {
            let appliances = {
                app_model: "4",
                app_brand: "ELECTROLUX",
                app_type_code: "SA001",
                app_voltage: "50",
                app_color: "red",
                app_states: [{"id":2,"name":"RUNNING"}]
            }
             chai.request(server)
                .post('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                })
        })
        it('should respond with a negative status code if the appliance state is RUNNING and OFF', (done) => {
            let appliances = {
                app_model: "5",
                app_brand: "ELECTROLUX",
                app_type_code: "SA001",
                app_voltage: "50",
                app_color: "red",
                app_states: [{"id":2,"name":"RUNNING"}, {"id":3,"name":"OFF"}]
            }
             chai.request(server)
                .post('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                });
        })
        it('should respond with a positive status code if the appliance is created', (done) => {
            let appliance = {
                app_model: "123",
                app_brand: "ELECTROLUX",
                app_type_code: "SA001",
                app_voltage: "50",
                app_color: "red",
                app_states: []
            }
            chai.request(server)
                .post('/api/appliances')
                .send(appliance)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(201);
                     done()
                });
        })
    })

   describe('Fetching appliance by id', () => {
        it('should respond with a negative status code if the appliance url incorrect', (done) => {
            chai.request(server)
                .get('/api/appliancess')
                .end((err, res) => {
                    chai.expect(res).to.have.status(404);
                    done()
                });
        })
        
        it('should respond with a positive status code if the appliance is available', (done) => {
            chai.request(server)
                .get('/api/appliances/app_model=123')
                .end((err, res) => {
                    chai.expect(res).to.have.status(200);
                    done()
                });
        })
    })

    
    describe('Editing appliance', () => {
        it('should respond with a negative status code if the appliance url incorrect', (done) => {
            chai.request(server)
                 .put('/api/appliancess')
                .end((err, res) => {
                    chai.expect(res).to.have.status(404);
                    done()
                });
        })
        it('should respond with a negative status code if the appliance model number is incorrect', (done) => {
            chai.request(server)
                .put('/api/appliances/id')
                .end((err, res) => {
                    chai.expect(res).to.have.status(404);
                    done()
                });
        })
        it('should respond with a negative status code if the appliance model number is empty', (done) => {
            let appliances = {
                app_model: "",
                app_brand: "ELECTROLUX",
                app_type_code: "SA001",
                app_voltage: "50",
                app_color: "red",
                app_states: []
            }
             chai.request(server)
                .put('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                });
        })
        it('should respond with a negative status code if the appliance brand name is empty', (done) => {
            let appliances = {
                app_model: "123",
                app_brand: "",
                app_type_code: "SA001",
                app_voltage: "50",
                app_color: "red",
                app_states: []
            }
             chai.request(server)
                .put('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                });
        })
        it('should respond with a negative status code if the appliance type is empty', (done) => {
            let appliances = {
                app_model: "123",
                app_brand: "ELECTROLUX",
                app_type_code: "",
                app_voltage: "50",
                app_color: "red",
                app_states: []
            }
             chai.request(server)
                .put('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                });
        })
        it('should respond with a negative status code if the appliance state is ON and OFF', (done) => {
            let appliances = {
                app_model: "123",
                app_brand: "ELECTROLUX",
                app_type_code: "SA001",
                app_voltage: "50",
                app_color: "red",
                app_states: [{"id":1,"name":"ON"}, {"id":3,"name":"OFF"}]
            }
             chai.request(server)
                .put('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                });
        })
        it('should respond with a negative status code if the appliance state is RUNNING', (done) => {
            let appliances = {
                app_model: "123",
                app_brand: "ELECTROLUX",
                app_type_code: "SA001",
                app_voltage: "50",
                app_color: "red",
                app_states: [{"id":2,"name":"RUNNING"}]
            }
             chai.request(server)
                .put('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                });
        })
        it('should respond with a negative status code if the appliance state is RUNNING and OFF', (done) => {
            let appliances = {
                app_model: "123",
                app_brand: "ELECTROLUX",
                app_type_code: "SA001",
                app_voltage: "50",
                app_color: "red",
                app_states: [{"id":2,"name":"RUNNING"}, {"id":3,"name":"OFF"}]
            }
             chai.request(server)
                .put('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                });
        })
        it('should respond with a positive status code if the appliance is edited', (done) => {
            let appliances = {
                app_model: "123",
                app_brand: "ELECTROLUX",
                app_type_code: "SA001",
                app_voltage: "50",
                app_color: "red",
                app_states: []
            }
             chai.request(server)
                .put('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(200);
                     done()
                });
        })
    })

   describe('Updating the status of the appliance', () => {
        it('should respond with a negative status code if the appliance url incorrect', (done) => {
            chai.request(server)
                 .patch('/api/appliancess')
                .end((err, res) => {
                    chai.expect(res).to.have.status(404);
                    done()
                });
        })
        it('should respond with a negative status code if the appliance id is incorrect', (done) => {
            chai.request(server)
                .patch('/api/appliances/app_model=0')
                .end((err, res) => {
                    chai.expect(res).to.have.status(404);
                    done()
                });
        })
        it('should respond with a negative status code if the appliance state is ON and OFF', (done) => {
            let appliances = {
                app_model: "123",
                app_states: [{"id":1,"name":"ON"}, {"id":3,"name":"OFF"}]
            }
             chai.request(server)
                .patch('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                });
        })
        it('should respond with a negative status code if the appliance state is RUNNING', (done) => {
            let appliances = {
                app_model: "123",
                app_states: [{"id":2,"name":"RUNNING"}]
            }
             chai.request(server)
                .patch('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                });
        })
        it('should respond with a negative status code if the appliance state is RUNNING and OFF', (done) => {
            let appliances = {
                app_model: "123",
                app_states: [{"id":2,"name":"RUNNING"}, {"id":3,"name":"OFF"}]
            }
             chai.request(server)
                .patch('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(400);
                     done()
                });
        })
        it('should respond with a positive status code if the appliance is updated', (done) => {
            let appliances = {
                app_model: "123",
                app_states: [{"id":3,"name":"OFF"}]
            }
             chai.request(server)
                .patch('/api/appliances')
                .send(appliances)
                 .end((err, res) => {
                     chai.expect(res).to.have.status(200);
                     done()
                });
        })
    })

    describe('Deleting appliance', () => {
        it('should respond with a negative status code if the appliance url incorrect', (done) => {
            chai.request(server)
                .delete('/api/appliancess')
                .end((err, res) => {
                    chai.expect(res).to.have.status(404);
                    done()
                });
        })
        it('should respond with a negative status code if the appliance id is incorrect', (done) => {
            chai.request(server)
                .delete('/api/appliances/test')
                .end((err, res) => {
                    chai.expect(res).to.have.status(404);
                    done()
                });
        })
        it('should respond with a 200 status code if the appliance is deleted', (done) => {
            let appliances = {
                app_model: "123",
            }
            chai.request(server)
                .delete('/api/appliances')
                .send({ app_model: "123" })
                .end((err, res) => {
                    chai.expect(res).to.have.status(200);
                    done()
                });
        })
    }) 
})
    