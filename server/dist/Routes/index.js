"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Types_controller_1 = __importDefault(require("../Controllers/Types.controller"));
const Appliances_controller_1 = __importDefault(require("../Controllers/Appliances.controller"));
const HttpStatus = __importStar(require("http-status-codes"));
exports.default = ({ app }) => {
    /*Seeded types data to DB*/
    app.post("/api/types", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const user = yield Types_controller_1.default.CreateTypes({
            type_code: "SA002",
            type_name: "Oven",
        });
        return res.send({ user });
    }));
    /*Get all appliances types*/
    app.get("/api/types", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const types = yield Types_controller_1.default.GetTypes();
            return res.status(HttpStatus.OK).send(types);
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send({ error });
        }
    }));
    /*Appliances: Routes*/
    /*Get all appliances*/
    app.get("/api/appliances", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const appliances = yield Appliances_controller_1.default.GetAppliances({
                app_model: "",
            });
            return res
                .status(appliances ? HttpStatus.OK : HttpStatus.NOT_FOUND)
                .send({ appliances });
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send({ error });
        }
    }));
    /*Get an appliance by its id*/
    app.get("/api/appliances/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const appliances = yield Appliances_controller_1.default.GetAppliances({
                app_model: req.params.id,
            });
            return res
                .status(appliances ? HttpStatus.OK : HttpStatus.NOT_FOUND)
                .send({ appliances });
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send({ error });
        }
    }));
    /*Create an appliance*/
    app.post("/api/appliances", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const appliance = yield Appliances_controller_1.default.CreateAppliances({
                app_model: req.body.app_model,
                app_brand: req.body.app_brand,
                app_type_code: req.body.app_type_code,
                app_voltage: req.body.app_voltage,
                app_color: req.body.app_color,
                app_states: req.body.app_states,
            });
            if (appliance) {
                return res.status(HttpStatus.CREATED).send({ appliance });
            }
            else {
                return res.status(HttpStatus.BAD_REQUEST).send({ message: "Error Occured." });
            }
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send({ error });
        }
    }));
    /*Update an appliance*/
    app.put("/api/appliances", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const response = yield Appliances_controller_1.default.ReplaceAppliances({
                app_model: req.body.app_model,
                app_brand: req.body.app_brand,
                app_type_code: req.body.app_type_code,
                app_voltage: req.body.app_voltage,
                app_color: req.body.app_color,
                app_states: req.body.app_states,
            });
            return res.status(response ? HttpStatus.OK : HttpStatus.NOT_FOUND).send({
                message: response
                    ? "Updated Successfully"
                    : "No record found to update.",
            });
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send({ error });
        }
    }));
    /*Modify some fields of appliances*/
    app.patch("/api/appliances", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const appliance = yield Appliances_controller_1.default.UpdateAppliances({
                app_model: req.body.app_model,
                app_brand: req.body.app_brand,
                app_type_code: req.body.app_type_code,
                app_voltage: req.body.app_voltage,
                app_color: req.body.app_color,
                app_states: req.body.app_states,
            });
            return res.status(HttpStatus.OK).send({ appliance });
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send({ error });
        }
    }));
    /*Delete appliances*/
    app.delete("/api/appliances", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const reponse = yield Appliances_controller_1.default.DeleteAppliances({
                app_model: req.body.app_model,
            });
            return res.status(reponse ? HttpStatus.OK : HttpStatus.NOT_FOUND).
                send({
                message: reponse
                    ? "Deleted Successfully"
                    : "No record found to delete!",
            });
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send({ error });
        }
    }));
};
