"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Appliances_model_1 = __importDefault(require("../Models/Appliances.model"));
/**
 * Get Appliances
 */
function GetAppliances({ app_model, }) {
    return __awaiter(this, void 0, void 0, function* () {
        return Appliances_model_1.default.find(app_model ? { app_model } : {})
            .then((data) => {
            return data;
        })
            .catch((error) => {
            throw error;
        });
    });
}
/**
 * Create appliances.
 */
function CreateAppliances({ app_model, app_brand, app_type_code, app_voltage, app_color, app_states, }) {
    return __awaiter(this, void 0, void 0, function* () {
        const validationRes = validateFormData(app_model, app_brand, app_type_code, app_voltage, app_color, app_states);
        if (validationRes.status === 400) {
            throw validationRes;
        }
        return Appliances_model_1.default.create({
            app_model,
            app_brand,
            app_type_code,
            app_voltage,
            app_color,
            app_states,
        })
            .then((data) => {
            return data;
        })
            .catch((error) => {
            throw error;
        });
    });
}
/**
 * Update appliances.
 */
function ReplaceAppliances({ app_model, app_brand, app_type_code, app_voltage, app_color, app_states, }) {
    return __awaiter(this, void 0, void 0, function* () {
        const validationRes = validateFormData(app_model, app_brand, app_type_code, app_voltage, app_color, app_states);
        if (validationRes.status === 400) {
            throw validationRes;
        }
        return Appliances_model_1.default.replaceOne({ app_model: app_model }, {
            app_model,
            app_brand,
            app_type_code,
            app_voltage,
            app_color,
            app_states,
        })
            .then((data) => {
            return data;
        })
            .catch((error) => {
            throw error;
        });
    });
}
/**
 * Update field of appliances
 */
function UpdateAppliances({ app_model, app_states, }) {
    return __awaiter(this, void 0, void 0, function* () {
        const validationRes = validateFormData(app_model, "", "", 0, "", app_states, true);
        if (validationRes.status === 400) {
            throw validationRes;
        }
        return Appliances_model_1.default.findOneAndUpdate({ app_model }, {
            app_states,
        }, { new: true })
            .then((data) => {
            return data;
        })
            .catch((error) => {
            throw error;
        });
    });
}
/**
 * Delete appliances.
 */
function DeleteAppliances({ app_model }) {
    return __awaiter(this, void 0, void 0, function* () {
        return Appliances_model_1.default.deleteOne({
            app_model,
        })
            .then((data) => {
            return data.deletedCount > 0;
        })
            .catch((error) => {
            throw error;
        });
    });
}
function validateFormData(app_model, app_brand, app_type_code, app_voltage, app_color, app_states, isModify = false) {
    if (!isModify && (!app_model || !app_brand || !app_type_code)) {
        return { status: 400, message: "Mandatory field is empty." };
    }
    else if (app_states && app_states.length > 0) {
        if (app_states.length > 2) {
            return { status: 400, message: "Form is not vaild." };
        }
        else if (app_states.length > 1 && app_states[0].name === "ON" && app_states[1].name === "OFF") {
            return { status: 400, message: "Form is not vaild." };
        }
        else if (app_states.length === 1 && app_states[0].name === "RUNNING") {
            return { status: 400, message: "Form is not vaild." };
        }
        else if (app_states.length >= 1 && app_states[0].name === "RUNNING") {
            return { status: 400, message: "Form is not vaild." };
        }
        else if (app_states.length > 1 && app_states[0].name === "OFF") {
            return { status: 400, message: "Form is not vaild." };
        }
    }
    return { status: 200, message: "Form is vaild." };
}
exports.default = {
    GetAppliances,
    CreateAppliances,
    ReplaceAppliances,
    UpdateAppliances,
    DeleteAppliances,
};
