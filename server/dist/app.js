"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
require("dotenv/config");
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const Routes_1 = __importDefault(require("./Routes"));
const connect_1 = __importDefault(require("./connect"));
const app = express_1.default();
const PORT = process.env.PORT;
const db = "mongodb+srv://smrtapp2o21user:BXq997nKYiScjQhq@smartappliance.zu6qy.mongodb.net/smartappliancesdb";
app.use(cors_1.default({ origin: "http://localhost:3000", optionsSuccessStatus: 200 }));
app.use(body_parser_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: true }));
connect_1.default({ db });
Routes_1.default({ app });
const server = app.listen(PORT, () => {
    console.log(`server is running on PORT ${PORT}`);
});
module.exports = server;
