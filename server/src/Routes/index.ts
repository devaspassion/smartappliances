import { RoutesInput } from "../types/route";
import TypesController from "../Controllers/Types.controller";
import AppliancesController from "../Controllers/Appliances.controller";
import * as HttpStatus from "http-status-codes";

export default ({ app }: RoutesInput) => {
  /*Seeded types data to DB*/
  app.post("/api/types", async (req, res) => {
    const user = await TypesController.CreateTypes({
      type_code: "SA002",
      type_name: "Oven",
    });
    return res.send({ user });
  });
    /*Get all appliances types*/
  app.get("/api/types", async (req, res) => {
    try {
      const types = await TypesController.GetTypes();
      return res.status(HttpStatus.OK).send(types);
    } catch (error) {
      return res.status(HttpStatus.BAD_REQUEST).send({ error });
    }
  });

/*Appliances: Routes*/
/*Get all appliances*/
  app.get("/api/appliances", async (req, res) => {
    try {
      const appliances = await AppliancesController.GetAppliances({
        app_model: "",
      });
      return res
        .status(appliances ? HttpStatus.OK : HttpStatus.NOT_FOUND)
        .send({ appliances });
    } catch (error) {
      return res.status(HttpStatus.BAD_REQUEST).send({ error });
    }
  });
/*Get an appliance by its id*/
  app.get("/api/appliances/:id", async (req, res) => {
    try {
      const appliances = await AppliancesController.GetAppliances({
        app_model: req.params.id,
      });
      return res
        .status(appliances ? HttpStatus.OK : HttpStatus.NOT_FOUND)
        .send({ appliances });
    } catch (error) {
      return res.status(HttpStatus.BAD_REQUEST).send({ error });
    }
  });
  /*Create an appliance*/
  app.post("/api/appliances", async (req, res) => {
    try {
      const appliance = await AppliancesController.CreateAppliances({
        app_model: req.body.app_model,
        app_brand: req.body.app_brand,
        app_type_code: req.body.app_type_code,
        app_voltage: req.body.app_voltage,
        app_color: req.body.app_color,
        app_states: req.body.app_states,
      });
        if (appliance) {
            return res.status(HttpStatus.CREATED).send({ appliance });
        } else {
            return res.status(HttpStatus.BAD_REQUEST).send({ message: "Error Occured." });
        }
    } catch (error) {
      return res.status(HttpStatus.BAD_REQUEST).send({ error });
    }
  });
/*Update an appliance*/
  app.put("/api/appliances", async (req, res) => {
    try {
      const response = await AppliancesController.ReplaceAppliances({
        app_model: req.body.app_model,
        app_brand: req.body.app_brand,
        app_type_code: req.body.app_type_code,
        app_voltage: req.body.app_voltage,
        app_color: req.body.app_color,
        app_states: req.body.app_states,
      });
      return res.status(response ? HttpStatus.OK : HttpStatus.NOT_FOUND).send({
        message: response
          ? "Updated Successfully"
          : "No record found to update.",
      });
    } catch (error) {
      return res.status(HttpStatus.BAD_REQUEST).send({ error });
    }
  });
/*Modify some fields of appliances*/
  app.patch("/api/appliances", async (req, res) => {
    try {
      const appliance = await AppliancesController.UpdateAppliances({
        app_model: req.body.app_model,
        app_brand: req.body.app_brand,
        app_type_code: req.body.app_type_code,
        app_voltage: req.body.app_voltage,
        app_color: req.body.app_color,
        app_states: req.body.app_states,
      });
      return res.status(HttpStatus.OK).send({ appliance });
    } catch (error) {
      return res.status(HttpStatus.BAD_REQUEST).send({ error });
    }
  });
/*Delete appliances*/
  app.delete("/api/appliances", async (req, res) => {
    try {
      const reponse = await AppliancesController.DeleteAppliances({
        app_model: req.body.app_model,
      });
        return res.status(reponse ? HttpStatus.OK : HttpStatus.NOT_FOUND).
            send({
                    message: reponse
                      ? "Deleted Successfully"
                      : "No record found to delete!",
                  });
    } catch (error) {
      return res.status(HttpStatus.BAD_REQUEST).send({ error });
    }
  });
};
