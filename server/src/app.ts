import express, { Application, Request, Response, NextFunction } from "express";
import "dotenv/config";
import cors from "cors";
import bodyParser from "body-parser";
import Routes from "./Routes";
import Connect from "./connect";

const app: Application = express();
const PORT = process.env.PORT;
const db =
  "mongodb+srv://smrtapp2o21user:BXq997nKYiScjQhq@smartappliance.zu6qy.mongodb.net/smartappliancesdb";

app.use(cors({ origin: "http://localhost:3000", optionsSuccessStatus: 200 }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

Connect({ db });
Routes({ app });

const server = app.listen(PORT, () => {
  console.log(`server is running on PORT ${PORT}`);
});

module.exports = server;
