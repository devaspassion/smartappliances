import Appliances, { IAppliances } from "../Models/Appliances.model";

interface IAppliancesInput {
  app_model: IAppliances["app_model"];
  app_brand: IAppliances["app_brand"];
  app_type_code: IAppliances["app_type_code"];
  app_voltage: IAppliances["app_voltage"];
  app_color: IAppliances["app_color"];
  app_states: IAppliances["app_states"];
}
interface IAppliancesKey {
  app_model?: IAppliances["app_model"];
}
interface Response {
    status: Number;
    message: String;
}
/**
 * Get Appliances
 */
async function GetAppliances({
  app_model,
}: IAppliancesKey): Promise<IAppliances> {
  return Appliances.find(app_model ? { app_model } : {})
    .then((data: any) => {
      return data;
    })
    .catch((error: Error) => {
      throw error;
    });
}
/**
 * Create appliances.
 */
async function CreateAppliances({
  app_model,
  app_brand,
  app_type_code,
  app_voltage,
  app_color,
  app_states,
}: IAppliancesInput): Promise<IAppliances> {
    const validationRes = validateFormData(app_model,
        app_brand,
        app_type_code,
        app_voltage,
        app_color,
        app_states);
    if (validationRes.status === 400) {
        throw validationRes;
    }
  return Appliances.create({
    app_model,
    app_brand,
    app_type_code,
    app_voltage,
    app_color,
    app_states,
  })
    .then((data: IAppliances) => {
      return data;
    })
    .catch((error: Error) => {
      throw error;
    });
}
/**
 * Update appliances.
 */
async function ReplaceAppliances({
  app_model,
  app_brand,
  app_type_code,
  app_voltage,
  app_color,
  app_states,
}: IAppliancesInput): Promise<IAppliances> {
    const validationRes = validateFormData(app_model,
        app_brand,
        app_type_code,
        app_voltage,
        app_color,
        app_states);
    if (validationRes.status === 400) {
        throw validationRes;
    }
  return Appliances.replaceOne(
    { app_model: app_model },
    {
      app_model,
      app_brand,
      app_type_code,
      app_voltage,
      app_color,
      app_states,
    }
  )
    .then((data: IAppliances) => {
      return data;
    })
    .catch((error: Error) => {
      throw error;
    });
}
/**
 * Update field of appliances
 */
async function UpdateAppliances({
  app_model,
  app_states,
}: IAppliancesInput): Promise<any> {
    const validationRes = validateFormData(app_model,
        "",
        "",
        0,
        "",
        app_states, true);
    if (validationRes.status === 400) {
        throw validationRes;
    }
  return Appliances.findOneAndUpdate(
    { app_model },
    {
      app_states,
    },
    { new: true }
  )
    .then((data: any) => {
      return data;
    })
    .catch((error: Error) => {
      throw error;
    });
}
/**
 * Delete appliances.
 */
async function DeleteAppliances({ app_model }: IAppliancesKey): Promise<any> {
  return Appliances.deleteOne({
    app_model,
  })
    .then((data: any) => {
      return data.deletedCount > 0;
    })
    .catch((error: Error) => {
      throw error;
    });
}

function validateFormData(
    app_model?: String,
    app_brand?: String ,
    app_type_code?: String,
    app_voltage?: Number,
    app_color?: String,
    app_states?: any[],
    isModify: Boolean = false
): { status: Number, message: String } {
    if (!isModify && (!app_model || !app_brand || !app_type_code)) {
        return { status: 400, message: "Mandatory field is empty." }
    } else if (app_states && app_states.length>0) {
        if (app_states.length > 2) {
            return { status: 400, message: "Form is not vaild." }
        } else if (app_states.length > 1 && app_states[0].name === "ON" && app_states[1].name === "OFF") {
            return { status: 400, message: "Form is not vaild." }
        }
        else if (app_states.length === 1 && app_states[0].name === "RUNNING") {
            return { status: 400, message: "Form is not vaild." }
        }else if (app_states.length >= 1 && app_states[0].name === "RUNNING") {
            return { status: 400, message: "Form is not vaild." }
        }
        else if (app_states.length > 1 && app_states[0].name === "OFF") {
            return { status: 400, message: "Form is not vaild." }
        }
    }
    return { status: 200, message: "Form is vaild." }
}

export default {
  GetAppliances,
  CreateAppliances,
  ReplaceAppliances,
  UpdateAppliances,
  DeleteAppliances,
};
