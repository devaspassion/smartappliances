import Types, { ITypes } from "../Models/Types.model";

interface ITypesInput {
  type_code: ITypes["type_code"];
  type_name: ITypes["type_name"];
}

/**
 * Create appliances types
 */
async function CreateTypes({
  type_code,
  type_name,
}: ITypesInput): Promise<ITypes> {
  return Types.create({
    type_code,
    type_name,
  })
    .then((data: ITypes) => {
      return data;
    })
    .catch((error: Error) => {
      throw error;
    });
}
/**
 * Get appliances types
 */
async function GetTypes(): Promise<ITypes> {
  return Types.find({})
    .then((data: ITypes) => {
      return data;
    })
    .catch((error: Error) => {
      throw error;
    });
}

export default {
  CreateTypes,
  GetTypes,
};
