import mongoose, { Schema, Document } from "mongoose";

export interface IAppliancesStates extends Document {
  name: String;
  id: Number;
}
export interface IAppliances extends Document {
  app_model: String;
  app_brand: String;
  app_type_code: String;
  app_voltage: Number;
  app_color: String;
  app_states: [];
}
/*Appliances DB Schema */
const AppliancesSchema: Schema = new Schema({
  app_model: {
    type: String,
    required: true,
    unique: true,
  },
  app_brand: {
    type: String,
    required: true,
  },
  app_type_code: {
    type: String,
    required: true,
  },
  app_voltage: {
    type: String,
  },
  app_color: {
    type: String,
  },
  app_states: {
    type: [],
  },
});

export default mongoose.model<IAppliances>("sa_appliances", AppliancesSchema);
