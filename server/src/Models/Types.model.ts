import mongoose, { Schema, Document } from "mongoose";

export interface ITypes extends Document {
  type_code: String;
  type_name: String;
}

/*Types DB Schema */
const TypesSchema: Schema = new Schema({
  type_code: {
    type: String,
    required: true,
    unique: true,
  },
  type_name: {
    type: String,
    required: true,
    unique: true,
  },
});

export default mongoose.model<ITypes>("sa_types", TypesSchema);
